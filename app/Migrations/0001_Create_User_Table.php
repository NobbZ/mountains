<?php
class DbMigration_1 extends DbMigrationsAppModel {

	public function up() {
		$sql = "CREATE TABLE `users` (`id` INT UNSIGNED NOT NULL AUTO_INCREMENT, `email` VARCHAR(255) NOT NULL, `nick` VARCHAR(20) NOT NULL, `passwordhash` VARCHAR(255) NOT NULL, PRIMARY KEY (`id`), UNIQUE INDEX `email_UNIQUE` (`email` ASC), UNIQUE INDEX `nick_UNIQUE` (`nick` ASC));";
		$this->query($sql);
	}

	public function down() {
		$sql = "DROP TABLE `users`;";
		$this->query($sql);
	}
}

